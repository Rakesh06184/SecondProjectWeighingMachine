package com.example.rakeshk.weighingmechinesampletest;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class SecondMainActivity extends AppCompatActivity {
    private static final String TAG = "welocme";

    private static final int USB_VENDOR_ID = 6790;
    private static final int USB_PRODUCT_ID = 29987;

    private PendingIntent mPermissionIntent;

    private UsbManager mUsbManager;
    private UsbDeviceConnection mConnection = null;
    private UsbSerialDevice mSerialDevice = null;

    private String buffer = "";
    private TextView mWeinghingMachineResult;

    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    /*private BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

            //If our device is detached, disconnect
            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                //if (mDevice != null && mDevice.equals(device)) {
                if (device != null && device.equals(device)) {
                    setDevice(null);
                }
            }

            //If a new device is attached, connect to it
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                mUsbManager.requestPermission(device, mPermissionIntent);
            }

            //If this is our permission request, check result
            if (ACTION_USB_PERMISSION.equals(action)) {
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false) && device != null) {

                    //Connect to the device
                    setDevice(device);
                } else {
                    setDevice(null);
                }
            }
        }
    };*/

    private UsbSerialInterface.UsbReadCallback callback = new UsbSerialInterface.UsbReadCallback() {

        @Override
        public void onReceivedData(byte[] data) {
            try {
                String dataUtf8 = new String(data, "UTF-8");
                buffer += dataUtf8;

                int index;
                while ((index = buffer.indexOf('\r')) != -1) {
                    final String dataStr = buffer.substring(0, index + 1).trim();
                    // Log.d("checks", "Serial dataStr: " + dataStr);
                    buffer = buffer.length() == index ? "" : buffer.substring(index + 1);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onSerialDataReceived(dataStr);
                        }
                    });
                }
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "Error receiving USB data", e);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWeinghingMachineResult = (TextView) findViewById(R.id.tv_weighing_machine);
        Intent intent = getIntent();
        String action = intent.getAction();

   //     UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> devices = mUsbManager.getDeviceList();

        for (UsbDevice device : devices.values()) {
            startSerialConnection(mUsbManager, device);
              }



       /*

        //Create the intent to fire with permission results
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);




        if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
            setDevice(device);
        } else {
            searchForDevice();
        }*/
    }

 /*   private void searchForDevice() {
        //If we find our device already attached, connect to it
        HashMap<String, UsbDevice> devices = mUsbManager.getDeviceList();
        UsbDevice selected = null;
        for (UsbDevice device : devices.values()) {
          //  if (device.getVendorId() == USB_VENDOR_ID && device.getProductId() == USB_PRODUCT_ID) {
                selected = device;
                break;
      //      }
        }

        //Request a connection
        if (selected != null) {
            if (mUsbManager.hasPermission(selected)) {
                setDevice(selected);
            } else {
                mUsbManager.requestPermission(selected, mPermissionIntent);
            }
        }
    }
*/
    private void setDevice(UsbDevice device) {

        if (device == null) {
            mConnection = null;
       //     stopUsbConnection();
            return;
        }

      //  if (device.getVendorId() == USB_VENDOR_ID && device.getProductId() == USB_PRODUCT_ID) {
            startSerialConnection(mUsbManager, device);
     //   }
    }
    void startSerialConnection(UsbManager usbManager, UsbDevice device) {
        UsbDeviceConnection connection = usbManager.openDevice(device);
        mSerialDevice = UsbSerialDevice.createUsbSerialDevice(device, connection);

        if (mSerialDevice != null && mSerialDevice.open()) {
            mSerialDevice.setBaudRate(9600);
            mSerialDevice.setDataBits(UsbSerialInterface.DATA_BITS_8);
            mSerialDevice.setStopBits(UsbSerialInterface.STOP_BITS_1);
            mSerialDevice.setParity(UsbSerialInterface.PARITY_NONE);
            mSerialDevice.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
            mSerialDevice.read(callback);
        }
    }

    //   StringBuilder strResult = new StringBuilder();

    private void onSerialDataReceived(String data) {

//        strResult.append(data + "\n");
        mWeinghingMachineResult.setText(data);
        // TODO: 29-11-2017 sending data to other activity(main activity)

      /*  Intent intent = new Intent();
        intent.putExtra("value", data);
        setResult(11, intent); //The data you want to send back
        finish();*/
    }

    @Override
    public void onResume() {
         super.onResume();
        //Register receiver for further events
     /*   IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // unregisterReceiver(mUsbReceiver);
     //   stopUsbConnection();
    }

    private void stopUsbConnection() {
        try {
            if (mSerialDevice != null) {
                mSerialDevice.close();
            }
            if (mConnection != null) {
                mConnection.close();
            }
        } finally {
            mSerialDevice = null;
            mConnection = null;
        }
    }
}
