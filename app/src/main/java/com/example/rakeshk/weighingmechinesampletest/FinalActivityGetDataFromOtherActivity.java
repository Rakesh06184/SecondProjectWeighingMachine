package com.example.rakeshk.weighingmechinesampletest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class FinalActivityGetDataFromOtherActivity extends AppCompatActivity {

    private TextView mResultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_get_data_from_other);

        mResultTextView = findViewById(R.id.tv_result);

        Intent intent = new Intent(FinalActivityGetDataFromOtherActivity.this ,SecondMainActivity.class);
        //    intent.putExtra("oldValue", "valueYouWantToChange");
        startActivityForResult(intent, 11); //I always put 0 for someIntValue
    }


        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            //Retrieve data in the intent
            if(requestCode == 11) {
                // TODO: 29-11-2017 get data frm second activity

                if(data.getExtras().getString("value") != null) {
                    String newValue = data.getStringExtra("value");
                    mResultTextView.setText(newValue);
                }
            }
        }

}
